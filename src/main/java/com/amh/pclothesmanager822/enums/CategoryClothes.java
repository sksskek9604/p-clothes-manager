package com.amh.pclothesmanager822.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CategoryClothes {

    TOP("상의"),
    BOTTOM("하의"),
    DRESS("드레스"),
    SHOES("신발"),
    ACCESSORY("악세서리");

    private final String name;



}
