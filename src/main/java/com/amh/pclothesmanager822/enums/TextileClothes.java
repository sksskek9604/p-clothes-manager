package com.amh.pclothesmanager822.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TextileClothes {

    COTTON("면"),
    SILK("실크"),
    POLYESTER("폴리에스터"),
    NYLON("나일론"),
    LINEN("린넨"),
    BLEND("혼방"),
    ETC("기타");
    private final String name;
}
