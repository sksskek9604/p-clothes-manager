package com.amh.pclothesmanager822.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OwnerClothes {

    MOTHER("엄마"),
    FATHER("아빠"),
    BROTHER("오빠"),
    ME("나"),
    GRANDMOTHER("할머니");

    private final String name;
}
