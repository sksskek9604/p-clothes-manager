package com.amh.pclothesmanager822;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PClothesManager822Application {

    public static void main(String[] args) {
        SpringApplication.run(PClothesManager822Application.class, args);
    }

}
