package com.amh.pclothesmanager822.entity;

import com.amh.pclothesmanager822.enums.CategoryClothes;
import com.amh.pclothesmanager822.enums.OwnerClothes;
import com.amh.pclothesmanager822.enums.TextileClothes;
import com.amh.pclothesmanager822.interfaces.CommonModelBuilder;
import com.amh.pclothesmanager822.model.ClothesRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Clothes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String nameClothes;
    @Column(nullable = false)
    private Double priceClothes;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TextileClothes textileClothes;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private CategoryClothes categoryClothes;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private OwnerClothes ownerClothes;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putClothes(ClothesRequest request) {
        this.nameClothes = request.getNameClothes();
        this.priceClothes = request.getPriceClothes();
        this.textileClothes = request.getTextileClothes();
        this.categoryClothes = request.getCategoryClothes();
        this.ownerClothes = request.getOwnerClothes();
        this.dateUpdate = LocalDateTime.now();
    }
    private Clothes(ClothesBuilder builder) {
        this.nameClothes = builder.nameClothes;
        this.priceClothes = builder.priceClothes;
        this.textileClothes = builder.textileClothes;
        this.categoryClothes = builder.categoryClothes;
        this.ownerClothes = builder.ownerClothes;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class ClothesBuilder implements CommonModelBuilder<Clothes> {
        private final String nameClothes;
        private final Double priceClothes;
        private final TextileClothes textileClothes;
        private final CategoryClothes categoryClothes;
        private final OwnerClothes ownerClothes;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public ClothesBuilder(ClothesRequest request) {
            this.nameClothes = request.getNameClothes();
            this.priceClothes = request.getPriceClothes();
            this.textileClothes = request.getTextileClothes();
            this.categoryClothes = request.getCategoryClothes();
            this.ownerClothes = request.getOwnerClothes();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }
        @Override
        public Clothes build() {
            return new Clothes(this);
        }
    }


}
