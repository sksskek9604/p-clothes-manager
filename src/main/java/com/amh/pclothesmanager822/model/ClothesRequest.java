package com.amh.pclothesmanager822.model;

import com.amh.pclothesmanager822.enums.CategoryClothes;
import com.amh.pclothesmanager822.enums.OwnerClothes;
import com.amh.pclothesmanager822.enums.TextileClothes;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ClothesRequest {

    @NotNull
    @Length(max = 20)
    @ApiModelProperty(notes = "옷 명칭", required = true)
    private String nameClothes;

    @NotNull
    @ApiModelProperty(notes = "옷 가격", required = true)
    private Double priceClothes;

    @Enumerated(EnumType.STRING)
    @NotNull
    @ApiModelProperty(notes = "옷 재질", required = true)
    private TextileClothes textileClothes;

    @Enumerated(EnumType.STRING)
    @NotNull
    @ApiModelProperty(notes = "옷 분류", required = true)
    private CategoryClothes categoryClothes;

    @Enumerated(EnumType.STRING)
    @NotNull
    @ApiModelProperty(notes = "옷 주인", required = true)
    private OwnerClothes ownerClothes;

}
