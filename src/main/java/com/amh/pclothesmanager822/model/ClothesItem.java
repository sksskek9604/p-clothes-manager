package com.amh.pclothesmanager822.model;

import com.amh.pclothesmanager822.entity.Clothes;
import com.amh.pclothesmanager822.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ClothesItem {

    @ApiModelProperty(notes = "옷 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "옷 명칭 + 옷 가격")
    private String fullName;
    @ApiModelProperty(notes = "옷 재질")
    private String textileClothes;
    @ApiModelProperty(notes = "옷 분류")
    private String categoryClothes;
    @ApiModelProperty(notes = "옷 주인")
    private String ownerClothes;
    @ApiModelProperty(notes = "등록날짜")
    private LocalDateTime dateCreate;
    @ApiModelProperty(notes = "수정날짜")
    private LocalDateTime dateUpdate;

    private ClothesItem(ClothesItemBuilder builder) {

        this.id = builder.id;
        this.fullName = builder.fullName;
        this.textileClothes = builder.textileClothes;
        this.categoryClothes = builder.categoryClothes;
        this.ownerClothes = builder.ownerClothes;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class ClothesItemBuilder implements CommonModelBuilder<ClothesItem> {
        private final Long id;
        private final String fullName;
        private final String textileClothes;
        private final String categoryClothes;
        private final String ownerClothes;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public ClothesItemBuilder(Clothes clothes) {
            this.id = clothes.getId();
            this.fullName = clothes.getNameClothes() + " " + String.format("%.0f",clothes.getPriceClothes()) + "원"; //String.format 소수점 몇째짜리까지?
            this.textileClothes = clothes.getTextileClothes().getName();
            this.categoryClothes = clothes.getCategoryClothes().getName();
            this.ownerClothes = "사랑하는" + clothes.getOwnerClothes().getName();
            this.dateCreate = clothes.getDateCreate();
            this.dateUpdate = clothes.getDateUpdate();
        }

        @Override
        public ClothesItem build() {
            return new ClothesItem(this);
        }
    }
}
