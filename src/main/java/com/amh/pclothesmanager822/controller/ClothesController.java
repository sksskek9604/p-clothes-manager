package com.amh.pclothesmanager822.controller;

import com.amh.pclothesmanager822.entity.Clothes;
import com.amh.pclothesmanager822.model.*;
import com.amh.pclothesmanager822.service.ClothesService;
import com.amh.pclothesmanager822.service.ResponseService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "옷 관리 시스템")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/clothes")
public class ClothesController {

    private final ClothesService clothesService;

    @ApiOperation(value = "옷 정보 등록")
    @PostMapping("/new")
    public CommonResult setClothes(@RequestBody @Valid ClothesRequest request) {
        clothesService.setClothes(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "옷 정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "옷 시퀀스", required = true)})
    @GetMapping("/{id}")
    public SingleResult<ClothesItem> getClothes(@PathVariable long id) {
        return ResponseService.getSingleResult(clothesService.getClothes(id));
    }

    @ApiOperation(value = "옷 정보 리스트")
    @GetMapping("/all")
    public ListResult<ClothesItem> getClothesAll() {
        return ResponseService.getListResult(clothesService.getClothesAll(), true);
    }

    @ApiOperation(value = "옷 정보 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "옷 시퀀스", required = true)})
    @PutMapping("/{id}")
    public CommonResult putClothes(@PathVariable long id, @RequestBody @Valid ClothesRequest request) {
        clothesService.putClothes(id, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "옷 정보 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "옷 시퀀스", required = true)})
    @DeleteMapping("/{id}")
    public CommonResult delClothes(@PathVariable long id) {
        clothesService.delClothes(id);
        return ResponseService.getSuccessResult();
    }
}
