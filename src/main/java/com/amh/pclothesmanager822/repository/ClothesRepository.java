package com.amh.pclothesmanager822.repository;

import com.amh.pclothesmanager822.entity.Clothes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClothesRepository extends JpaRepository<Clothes, Long> {
}
