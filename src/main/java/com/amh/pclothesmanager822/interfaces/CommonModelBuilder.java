package com.amh.pclothesmanager822.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
