package com.amh.pclothesmanager822.service;

import com.amh.pclothesmanager822.entity.Clothes;
import com.amh.pclothesmanager822.exception.CMissingDataException;
import com.amh.pclothesmanager822.model.ClothesItem;
import com.amh.pclothesmanager822.model.ClothesRequest;
import com.amh.pclothesmanager822.model.ListResult;
import com.amh.pclothesmanager822.repository.ClothesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ClothesService {

    private final ClothesRepository clothesRepository;

    public void setClothes(ClothesRequest request) {
        Clothes clothes = new Clothes.ClothesBuilder(request).build();
        clothesRepository.save(clothes);
    }

    public ClothesItem getClothes(long id) {
        Clothes clothes = clothesRepository.findById(id).orElseThrow(CMissingDataException::new);
        ClothesItem result = new ClothesItem.ClothesItemBuilder(clothes).build();

        return result;
    }

    public ListResult<ClothesItem> getClothesAll() {
        List<ClothesItem> result = new LinkedList<>();

        List<Clothes> clothes = clothesRepository.findAll();

        for (Clothes clothes1 : clothes) {
            ClothesItem addItem = new ClothesItem.ClothesItemBuilder(clothes1).build();
            result.add(addItem);
        }
        return ListConvertService.settingResult(result);
    }

    public void putClothes(long id, ClothesRequest request) {
        Clothes clothes = clothesRepository.findById(id).orElseThrow(CMissingDataException::new);
        clothes.putClothes(request);

        clothesRepository.save(clothes);
    }

    public void delClothes(long id) {
        clothesRepository.deleteById(id);
    }
}
